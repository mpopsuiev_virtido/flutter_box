import 'dart:async';
import 'dart:convert';

// import 'package:g_captcha/g_captcha.dart';
import 'package:flutter/material.dart';
import 'package:flutter_box/recaptcha.dart';
import 'package:flutter_box/lib.dart';



void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  MyApp({Key key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Future<CaptchaCredentials> futureCaptchaCredentials;

  String verifyResult = "";
  RecaptchaV2Controller recaptchaV2Controller = RecaptchaV2Controller();

  @override
  void initState() {
    super.initState();
    futureCaptchaCredentials = getCaptchaParams();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Fetch Data Example',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Fetch Data Example'),
        ),
        body: Center(
          child: FutureBuilder<CaptchaCredentials>(
            future: futureCaptchaCredentials,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                // GCaptcha.reCaptcha(snapshot.data.recaptchaStoken).then(
                //   (value) {
                //     print('tokenResult: $value');
                //   },
                // );

                return Stack(
                  children: <Widget>[
                    Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          RaisedButton(
                            child: Text("SHOW ReCAPTCHA"),
                            onPressed: () {
                              recaptchaV2Controller.show();
                            },
                          ),
                          Text(verifyResult),
                        ],
                      ),
                    ),
                    RecaptchaV2(
                      apiKey: snapshot.data.recaptchaSiteKey,
                      // apiSecret: snapshot.data.recaptchaStoken,
                      pluginURL:
                          "https://auth-wr3pcyvllq-oa.a.run.app/v1/auth/recaptcha",
                      controller: recaptchaV2Controller,
                      onVerifiedError: (err, response) {
                        print(err);
                        print(response);
                      },
                      onVerifiedSuccessfully: (success, response) {
                        print(response);
                        setState(() async {
                          var result = await requestSMS(response);
                          print(result);

                          if (success) {
                            verifyResult =
                                "You've been verified successfully.\n Token" +
                                    result;
                          } else {
                            verifyResult =
                                "Failed to verify.\n Token" + result;
                          }
                        });
                      },
                    ),
                  ],
                );
              } else if (snapshot.hasError) {
                return Text("${snapshot.error}");
              }

              // By default, show a loading spinner.
              return CircularProgressIndicator();
            },
          ),
        ),
      ),
    );
  }
}
