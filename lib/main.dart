import 'package:flutter/material.dart';
import 'package:flutter_box/lib.dart';
import 'package:flutter_box/recaptcha.dart';

void main() {
  runApp(MaterialApp(
    title: 'Navigation Basics',
    home: Home(),
  ));
}

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('First Route'),
      ),
      body: Center(
        child: ElevatedButton(
          child: Text('Login'),
          onPressed: () async {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => CaptchaPage()),
            );
          },
        ),
      ),
    );
  }
}

class CaptchaPage extends StatefulWidget {
  CaptchaPage({Key key}) : super(key: key);

  @override
  _CaptchaPageState createState() => _CaptchaPageState();
}

class _CaptchaPageState extends State<CaptchaPage> {
  Future<CaptchaCredentials> futureCaptchaCredentials;

  String verifyResult = "";
  String phone = "+380666813469";
  RecaptchaV2Controller recaptchaV2Controller = RecaptchaV2Controller();

  String sessionInfo;

  @override
  void initState()  {
    super.initState();
    futureCaptchaCredentials = getCaptchaParams();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Fetch Data Example',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Fetch Data Example'),
        ),
        body: Center(
          child: FutureBuilder<CaptchaCredentials>(
            future: futureCaptchaCredentials,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                // GCaptcha.reCaptcha(snapshot.data.recaptchaStoken).then(
                //   (value) {
                //     print('tokenResult: $value');
                //   },
                // );

                return Stack(
                  children: <Widget>[
                    Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          TextFormField(
                            keyboardType: TextInputType.phone,
                            autocorrect: false,
                            // ignore: missing_return
                            validator: (value) {
                              if (value.isEmpty || value.length < 10) {
                                return 'Please enter valid number';
                              }
                            },
                            onSaved: (String value) {
                              phone = value;
                            },
                            maxLines: 1,
                            decoration: new InputDecoration(
                                labelText: 'Enter your number',
                                hintText: 'Enter a number',
                                icon: const Icon(Icons.phone),
                                labelStyle: new TextStyle(
                                    decorationStyle:
                                        TextDecorationStyle.solid)),
                          ),
                          RaisedButton(
                            child: Text("SHOW ReCAPTCHA"),
                            onPressed: () {
                              recaptchaV2Controller.show();
                            },
                          ),
                          RaisedButton(
                            child: Text("Go Back"),
                            onPressed: () async {
                              Navigator.pop(context);
                            },
                          ),
                          Text(verifyResult),
                        ],
                      ),
                    ),
                    RecaptchaV2(
                      apiKey: snapshot.data.recaptchaSiteKey,
                      // apiSecret: captchaCredentials.recaptchaStoken,
                      pluginURL:
                          "https://auth-wr3pcyvllq-oa.a.run.app/v1/auth/recaptcha",
                      controller: recaptchaV2Controller,
                      onVerifiedError: (err, response) {
                        print(err);
                        print(response);
                        Navigator.pop(context);
                      },
                      onVerifiedSuccessfully: (success, recaptchaToken) {
                        print(recaptchaToken);
                        setState(() async {
                          sessionInfo = await requestSMS(recaptchaToken);
                          print(sessionInfo);

                          if (success) {
                            verifyResult =
                                "You've been verified successfully.\n Token" +
                                    sessionInfo;
                            recaptchaV2Controller.hide();
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => LoginPage(sessionInfo)),
                            );
                          } else {
                            verifyResult =
                                "Failed to verify.\n Token" + sessionInfo;
                            recaptchaV2Controller.hide();
                            Navigator.pop(context);
                          }
                        });
                      },
                    ),
                  ],
                );
              } else if (snapshot.hasError) {
                return Text("${snapshot.error}");
              }

              // By default, show a loading spinner.
              return CircularProgressIndicator();
            },
          ),
        ),
      ),
    );
  }
}

class SecondRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Second Route"),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text('Go back!'),
        ),
      ),
    );
  }
}

class LoginPage extends StatefulWidget {
 final String sessionInfo;

  LoginPage(this.sessionInfo, {Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState(sessionInfo);
}

class _LoginPageState extends State<LoginPage> {
  _LoginPageState(this.sessionInfo);

  String sessionInfo;

  String code;

  String verifyResult;

  @override
  void initState()  {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Fetch Data Example',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Fetch Data Example'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              TextFormField(
                keyboardType: TextInputType.number,
                autocorrect: false,
                // ignore: missing_return
                validator: (value) {
                  if (value.isEmpty || value.length < 1) {
                    return 'Please enter valid code';
                  }
                },
                onSaved: (String value) {
                  code = value;
                },
                maxLines: 1,
                decoration: new InputDecoration(
                    labelText: 'Enter code from SMS',
                    hintText: 'Enter code from SMS',
                    icon: const Icon(Icons.phone),
                    labelStyle: new TextStyle(
                        decorationStyle: TextDecorationStyle.solid)),
              ),
              RaisedButton(
                child: Text("Finish"),
                onPressed: () async {
                  var response = await verifySMSCode(code, sessionInfo);
                  print(response);
                  verifyResult = response.toString();
                },
              ),
              RaisedButton(
                child: Text("Go Back"),
                onPressed: () async {
                  Navigator.pop(context);
                },
              ),
              Text("done"),
            ],
          ),
        ),
      ),
    );
  }
}
