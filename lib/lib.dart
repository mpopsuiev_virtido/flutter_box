import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class CaptchaCredentials {
  final String kind;
  final String recaptchaStoken;
  final String recaptchaSiteKey;

  CaptchaCredentials({this.kind, this.recaptchaStoken, this.recaptchaSiteKey});

  factory CaptchaCredentials.fromJson(Map<String, dynamic> json) {
    return CaptchaCredentials(
      kind: json['kind'],
      recaptchaStoken: json['recaptchaStoken'],
      recaptchaSiteKey: json['recaptchaSiteKey'],
    );
  }
}

Future<CaptchaCredentials> getCaptchaParams() async {
  final response = await http.get(
      'https://auth-wr3pcyvllq-oa.a.run.app/v1/auth/login/request_captcha');

  if (response.statusCode == 200) {
    return CaptchaCredentials.fromJson(jsonDecode(response.body));
  } else {
    throw Exception('Failed to load CaptchaCredentials');
  }
}

Future<String> requestSMS(String recaptchaToken) async {
  var response = await http.post(
    'https://auth-wr3pcyvllq-oa.a.run.app/v1/auth/login/send_sms_code',
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, String>{
      'recaptchaToken': recaptchaToken,
      "phone": "+380666813469",
    }),
  );
  print(response.body);
  Map<String, dynamic> sessionInfo = jsonDecode(response.body);
  if (response.statusCode == 200) {
    print(sessionInfo["sessionInfo"]);
    return sessionInfo["sessionInfo"] as String;
  } else {
    throw Exception('Failed to requestSMS');
  }
}

Future<dynamic> verifySMSCode(String code, String sessionInfo) async {
  var response = await http.post(
    'https://auth-wr3pcyvllq-oa.a.run.app/v1/auth/login/with_sms_code',
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, String>{
      'code': code,
      "sessionInfo": sessionInfo,
    }),
  );

  print(response.body);
  Map<String, dynamic> data = jsonDecode(response.body);
  print(data);
  if (response.statusCode == 200) {
    return data;
  } else {
    throw Exception('Failed to verifySMSCode');
  }
}
